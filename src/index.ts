const readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout);

//1 - Convertisseur de devises
/*
    Créer une fonction qui prend en paramètre :
    - une valeur,
    - une devise d'entrée
    - une devise de sortie

    SI la devise d'entrée est en EUR et la devise de sortie est en CAD
        ALORS la devise d'entrée est multipliée par 1.5
    SINON SI la devise d'entrée est en EUR et la devise de sortie est en JPY
        ALORS la devise d'entrée est multipliée par 130
    SINON la devise d'entrée ne change pas

    SI la devise d'entrée est en CAD et la devise de sortie est en EUR
        ALORS la devise d'entrée est multipliée par 0.67
    SINON SI la devise d'entrée est en CAD et la devise de sortie est en JPY
        ALORS la devise d'entrée est multipliée par 87
    SINON la devise d'entrée ne change pas

    SI la devise d'entrée est en JPY et la devise de sortie est en CAD
        ALORS la devise d'entrée est multipliée par 0.0115
    SINON SI la devise d'entrée est en JPY et la devise de sortie est en EUR
        ALORS la devise d'entrée est multipliée par 0.0077
    SINON la devise d'entrée ne change pas
*/

//Intêrét de la convertion d'une devise dans sa devise? du coup je l'ai pas mis
const moneyConverter = (
  value: number,
  entranceCurrency: string,
  exitCurrency: string,
) => {
  let result: string;

  if (entranceCurrency === 'EUR') {
    result =
      exitCurrency === 'CAD'
        ? `${(value * 1.5).toFixed(2)} CAD`
        : `${(value * 130).toFixed(2)} JPY`;
  } else if (entranceCurrency === 'CAD') {
    result =
      exitCurrency === 'EUR'
        ? `${(value * 0.67).toFixed(2)} EUR`
        : `${(value * 87).toFixed(2)} JPY`;
  } else if (entranceCurrency === 'JPY') {
    result =
      exitCurrency === 'EUR'
        ? `${(value * 0.0077).toFixed(2)} EUR`
        : `${(value * 0.0115).toFixed(2)} CAD`;
  } else {
    throw new Error('La devise d\'entrée n\'est pas correcte')
  }
  return result;
};

// console.log(moneyConverter(3, 'CAD', 'EUR'));

//2 - Calculateur de frais de livraison
/*

    Créer une variable initialPrice = 0;
    Créer une variable supplement = 0;

//-------------------------------------------------------------------------

    Créer une fonction deliveryPrice qui prend en paramètre :
    - un weight
    - une length
    - une width
    - une height
    - une destination

    Définir une variable dimension = length + width + height

    SI le pays de destination est "destination"
        SI le "poids" est <= 1kg
            SI dimension > 150cm
                countryPrice(destination) + packDimension(destination)
            SINON countryPrice(destination)
        SINON SI le "poids" est > 1kg && <= 3kg
            SI dimension > 150cm
                (countryPrice(destination) * 2) + packDimension(destination)
            SINON countryPrice(destination) * 2
        SINON SI le "poids" est > 3 kg
            SI dimension > 150cm
                (countryPrice(destination) * 3) + packDimension(destination)
            SINON countryPrice(destination) * 3
        SINON afficher "merci d'indiquer un poids" //throw new Error('...')

//-------------------------------------------------------------------------

    Créer une fonction countryPrice qui prend en paramètre
    - une destination
        SI destination = france ALORS initialPrice = 10;
        SINON SI destination = canada ALORS initialPrice = 15;
        SINON SI destination = japon ALORS initialPrice = 1000;

//-------------------------------------------------------------------------

    Créer une fonction packDimension qui prend en paramètre:
    - destination
        SI destination = france ALORS supplement = 5;
        SINON SI destination = canada ALORS supplement = 7.5;
        SINON SI destination = japon ALORS supplement = 500;
*/

let initialPrice: number;
let supplement: number;

const deliveryPrice = (
  weight: number,
  length: number,
  width: number,
  height: number,
  destination: string,
): number => {
  const dimension = length + width + height;
  let price: number = 0;

  if (destination === destination) {
    if (weight <= 1) {
      price =
        dimension > 150
          ? countryPrice(destination) + packDimension(destination)
          : countryPrice(destination);
    } else if (weight > 1 && weight <= 3) {
      price =
        dimension > 150
          ? countryPrice(destination) * 2 + packDimension(destination)
          : countryPrice(destination) * 2;
    } else if (weight > 3) {
      price =
        dimension > 150
          ? countryPrice(destination) * 3 + packDimension(destination)
          : countryPrice(destination) * 3;
    } else {
      throw new Error("merci d'indiquer un poids");
    }
  }
  return price;
};

const countryPrice = (destination: string): number => {
  switch (destination) {
    case 'France':
      initialPrice = 10;
      break;
    case 'Canada':
      initialPrice = 15;
      break;
    case 'Japon':
      initialPrice = 1000;
      break;
    default:
      console.log('Saisissez un pays');
  }
  return initialPrice;
};

const packDimension = (destination: string): number => {
  switch (destination) {
    case 'France':
      supplement = 5;
      break;
    case 'Canada':
      supplement = 7.5;
      break;
    case 'Japon':
      supplement = 500;
      break;
    default:
      console.log('Saisissez un pays');
  }
  return supplement;
};

// console.log(deliveryPrice(5, 20, 49, 250, 'Canada'));

//3 - Calculateur de frais de douanes
/*
    Créer une fonction calculateDuties qui prend pour paramètre
    - un pays de destination
    (deliveryPrice(5, 20, 49, 250, 'Canada')) * (1 + (duties(destination))/ 100)

    Créer une fonction duties qui prend pour paramètre:
    - une destination

    Créer une variable duty = 0;

    SI destination = Canada 
        ALORS duty = 15;
    SI destination = Japon 
        ALORS duty = 10;
    SI destination = France 
        ALORS duty = 0;
*/
const calculateDuties = (value: number, destination: string) => {
  return value * (1 + duties(destination) / 100) - value;
};

let duty: number;
const duties = (destination: string): number => {
  switch (destination) {
    case 'Canada':
      duty = 15;
      break;
    case 'Japon':
      duty = 10;
      break;
    case 'France':
      duty = 0;
      break;
    default:
      console.log('Veuillez saisir un pays');
  }
  return duty;
};

rl.question("Quelle est la devise d'entrée ?", (entranceCurrency: string) => {
  //Devise d'entrée inexistante
  if (
    entranceCurrency !== 'EUR' &&
    entranceCurrency !== 'CAD' &&
    entranceCurrency !== 'JPY'
  ) {
    throw new Error("Cette devise n'existe pas");
  }
  rl.question('Quelle est la devise de sortie? ', (exitCurrency: string) => {
    //Devise de sortie inexistante
    if (
      exitCurrency !== 'EUR' &&
      exitCurrency !== 'CAD' &&
      exitCurrency !== 'JPY'
    ) {
      throw new Error("Cette devise n'existe pas");
    }
    rl.question('Quelle valeur voulez-vous convertir? ', (value: number) => {
      //Mauvaise valeur indiquée
      if (value <= 0 || value === undefined || value === null || isNaN(value)) {
        throw new Error('Veuillez corriger la valeur indiquée');
      }

      console.log(
        'Le résultat est :' +
          moneyConverter(value, entranceCurrency, exitCurrency),
      );
      rl.question('Quel est le poids de votre colis? ', (weight: number) => {
        if (weight <= 0 || weight > 100) {
          throw new Error("Le poids de ce colis n'est pas accepté");
        }
        rl.question('Votre colis mesure en longueur ? ', (length: number) => {
          if (length <= 0 || length > 100) {
            throw new Error("La longueur de ce colis n'est pas accepté");
          }
          rl.question('Votre colis mesure en largeur? ', (width: number) => {
            if (width <= 0 || width > 100) {
              throw new Error("La largeur de ce colis n'est pas accepté");
            }
            rl.question('Votre colis mesure en hauteur', (height: number) => {
              if (height <= 0 || height > 100) {
                throw new Error("La hauteur de ce colis n'est pas accepté");
              }
              rl.question(
                'Quel est le pays de destination? ',
                (destination: string) => {
                  if (
                    destination !== 'France' &&
                    destination !== 'Canada' &&
                    destination !== 'Japon'
                  ) {
                    throw new Error("Cette destination n'est pas acceptée");
                  }
                  console.log(
                    'Votre colis coûtera :' +
                      deliveryPrice(weight, length, width, height, destination),
                  );
                  rl.question(
                    'Quelle est la valeur du colis? :',
                    (value: number) => {
                      if (
                        value <= 0 ||
                        value === undefined ||
                        value === null ||
                        isNaN(value)
                      ) {
                        throw new Error('Veuillez corriger la valeur indiquée');
                      }
                      rl.question(
                        'Quel est le pays de destination? :',
                        (destination: string) => {
                          if (
                            destination !== 'France' &&
                            destination !== 'Canada' &&
                            destination !== 'Japon'
                          ) {
                            throw new Error(
                              "Cette destination n'est pas acceptée",
                            );
                          }
                          console.log(
                            'Les frais de douanes seront de :' +
                              calculateDuties(value, destination).toFixed(2),
                          );
                          rl.close();
                        },
                      );
                    },
                  );
                },
              );
            });
          });
        });
      });
    });
  });
});
// console.log(calculateDuties(23, 'Canada').toFixed(2));
