# Installation du projet 
Lancer la commande suivante pour installer les dépendances du projet avec les mêmes versions.
```bash
npm ci
```

Gestion du code source TypeScript dans le dossier src.
Génération du code js dans le dossier dist.

## Côté scripts
Pour faciliter le développement du projet, utilisez les commandes suivantes: 
- Lancer Nodemon
    ```bash 
    npm run dev
    ```

- Lancer le dossier dist
    ```bash 
    npm start
    ```

- Transpiler le projet 
    ```bash 
    npm run build
    ```

